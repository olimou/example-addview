package addlines.example.com.olimou.addlines.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout;

import addlines.example.com.olimou.addlines.R;

public class EditButtonGroup extends LinearLayout {
    private EditText editText;
    private AppCompatImageButton btnDelete;

    public EditButtonGroup(Context context) {
        super(context);

        init();
    }

    public EditButtonGroup(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public EditButtonGroup(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init();
    }

    public EditButtonGroup(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        init();
    }

    public void init() {
        inflate(getContext(), R.layout.customview_edit_button, this);

        editText = findViewById(R.id.input_name);
        btnDelete = findViewById(R.id.btn_delete);
    }

    public void setOnDeleteClickListener(OnClickListener onClickListener) {
        btnDelete.setOnClickListener(onClickListener);
    }

    public String getValue() {
        return editText.getText().toString();
    }
}
