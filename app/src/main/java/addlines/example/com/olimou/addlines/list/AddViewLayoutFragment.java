package addlines.example.com.olimou.addlines.list;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import addlines.example.com.olimou.addlines.R;
import addlines.example.com.olimou.addlines.view.EditButtonGroup;

public class AddViewLayoutFragment extends Fragment {

    private ViewGroup llContainer;

    public AddViewLayoutFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_view_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        AppCompatButton btnAdd = view.findViewById(R.id.btn_add);
        AppCompatButton btnShow = view.findViewById(R.id.btn_show);
        llContainer = view.findViewById(R.id.ll_container);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewLine();
            }
        });

        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChildValues();
            }
        });
    }

    private void addNewLine() {
        final EditButtonGroup buttonGroup = new EditButtonGroup(getContext());

        buttonGroup.setOnDeleteClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeView(buttonGroup);
            }
        });

        addView(buttonGroup);
    }

    private void showChildValues() {
        int childCount = llContainer.getChildCount();

        StringBuilder stringBuilder = new StringBuilder();

        if (childCount > 0) {
            stringBuilder.append("Os valores são: \n");

            for (int i = 0; i < childCount; i++) {
                EditButtonGroup buttonGroup = (EditButtonGroup) llContainer.getChildAt(i);

                if (buttonGroup != null) {

                    stringBuilder
                            .append(buttonGroup.getValue())
                            .append("\n");
                }
            }
        } else {
            stringBuilder.append("Não há valores");
        }

        new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.app_name))
                .setMessage(stringBuilder.toString())
                .setPositiveButton(getString(R.string.ok), null)
                .show();
    }

    private void addView(EditButtonGroup buttonGroup) {
        llContainer.addView(buttonGroup);
        llContainer.invalidate();
        buttonGroup.requestFocus();
    }

    private void removeView(EditButtonGroup buttonGroup) {
        llContainer.removeView(buttonGroup);
        llContainer.invalidate();
    }
}
